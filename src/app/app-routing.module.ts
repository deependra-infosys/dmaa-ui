import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserAdministrationComponent } from './user-administration/user-administration.component';

const routes: Routes = [{
  path:'user-administration',component: UserAdministrationComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
